"""
    Tasks
"""

from app.api.items.entities.utils import choice_entity
from app.celeryapp.celery_holder import celery


@celery.task(name='app.save_data')
def save_data(format_file, url_file, encode, separator):
    entity = choice_entity(format_file, url_file, encode, separator)
    df = entity.read_file()
    entity.iterate_df(df)
    return True


if __name__ == '__main__':
    entity = save_data(format_file='csv', url_file='technical_challenge_data.csv', encode="utf8", separator=',')