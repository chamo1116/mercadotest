"""
Api application file
"""

# local imports
from app.api.items.routes import items_bp


def init_app(app):
    """
    initialize Api app
    """
    app.register_blueprint(items_bp, url_prefix='/api/items')
