"""
Schemas for the Api app
"""

from app.db import marshmallow


class ItemsSchema(marshmallow.Schema):
    """
    This class represents the item schema.
    """
    class Meta:
        fields = (
            'site', 'id', 'price', 'start_time', 'name', 'description', 'nick_name', 'created_on', 'updated_on'
        )


item_schema = ItemsSchema()
