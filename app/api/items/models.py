"""
Models for the Api app
"""

# local imports
from app.db import db


class Items(db.Model):
    """
    This class represents the items table.
    """

    # Ensures table will be named in plural and not in singular as in
    # the name of the model
    __tablename__ = 'items'

    site = db.Column(db.String(length=3))
    id = db.Column(db.Integer(), primary_key=True)
    price = db.Column(db.Integer(), nullable=True)
    start_time = db.Column(db.String(length=255), nullable=True)
    name = db.Column(db.String(length=255), nullable=True)
    description = db.Column(db.String(length=255), nullable=True)
    nick_name = db.Column(db.String(length=255), nullable=True)
    created_on = db.Column(db.DateTime, index=True, server_default=db.func.now())
    updated_on = db.Column(db.DateTime, server_default=db.func.now(), onupdate=db.func.now())

    def __repr__(self):
        return '<id: {}>'.format(self.id)

    def save(self):
        db.session.add(self)
        db.session.commit()
