"""
Views for the Api app
"""

# third-party imports
from flask_restful import Resource, reqparse, request
import celery.states as states

# local imports
from app.utils import response_json
from app import constants
from .schemas import item_schema
from app.tasks import save_data
from celery.result import AsyncResult

parser = reqparse.RequestParser()  # pylint: disable=invalid-name
parser.add_argument('format_file', type=str, required=True, help='format of file')
parser.add_argument('separator', type=str, required=True, help='how the data is separated')
parser.add_argument('encode', type=str, required=True, help='encode mode')
parser.add_argument('url_file', type=str, required=True,  help='url or path of file')


class ItemResource(Resource):
    """
    This class represents Item
    """

    def post(self):
        # parse arguments given in the post call
        args = parser.parse_args()
        format_file = args["format_file"]
        separator = args["separator"]
        encode = args["encode"]
        url_file = args["url_file"]
        task = save_data.delay(format_file, url_file, encode, separator)
        return response_json(True, {'task_id': task.id}, constants.TEXT_OPERATION_PROCESSING)

    def get(self):
        args = request.args  # retrieve args from query string
        if 'task_id' in args:
            res = AsyncResult(args['task_id'])
            if res.state == states.PENDING:
                return response_json(True, {'result': str(res.result)}, constants.TEXT_OPERATION_PROCESSING)
            elif res.state == states.SUCCESS:
                return response_json(True, {'result': str(res.result)}, constants.TEXT_OPERATION_SUCCESSFUL)
            else:
                return response_json(False, {'result': str(res.result)}, constants.TEXT_OPERATION_UNSUCCESSFUL)
