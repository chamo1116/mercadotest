

from app.api.items.entities.Csv import Csv

ENTITY_INSTANCES = {
    'csv': Csv
}


def choice_entity(entity, file_name, encode, delimiter):
    entity = ENTITY_INSTANCES.get(entity)
    entity = entity(file_name, encode, delimiter)
    return entity
