"""
Common methods used by entities.
"""

import os
import requests

from app.api.items.models import Items


class CommonEntities:

    @staticmethod
    def refresh_token_mercado():
        access_token = os.getenv('ACCESS_TOKEN_MERCADO',
                                 'APP_USR-2188707946741860-113000-7895d5e34cc93ef3c36431cd1a181911-152854254')
        refresh_token = os.getenv('REFRESH_TOKEN_MERCADO', 'TG-5fc440eff4ea0b00062943cb-152854254')
        app_id = os.getenv('APPID_MERCADO', '2188707946741860')
        secret_key = os.getenv('SECRET_KEY_MERCADO', 'FzxiUT2c9OYpUnZurzo5YLEuqJifg52a')
        url = "https://api.mercadolibre.com/oauth/token"
        payload = dict(grant_type='refresh_token', client_id=app_id, client_secret=secret_key,
                       refresh_token=refresh_token)
        headers = {'accept': 'application/json', 'content-type': 'application/x-www-form-urlencoded'}
        r = requests.post(url, data=payload, headers=headers).json()
        if 'access_token' in r:
            access_token = r["access_token"]
        return access_token

    @staticmethod
    def item_request(code, access_token):
        r = list()
        url = 'https://api.mercadolibre.com/items?ids={}&access_token={}'.format(code, access_token)
        r = requests.get(url).json()
        if r:
            r = r[0]["body"]
        return r

    @staticmethod
    def category_request(category_id, access_token):
        url = 'https://api.mercadolibre.com//categories/{}'.format(category_id)
        headers = {'Authorization': 'Bearer {}'.format(access_token)}
        r = requests.get(url, headers=headers).json()
        if r and 'name' in r:
            r = r["name"]
        return r

    @staticmethod
    def currency_request(currency_id, access_token):
        url = 'https://api.mercadolibre.com/currencies/{}'.format(currency_id)
        headers = {'Authorization': 'Bearer {}'.format(access_token)}
        r = requests.get(url, headers=headers).json()
        if r and 'description' in r:
            r = r["description"]
        return r

    @staticmethod
    def seller_request(seller_id, access_token):
        url = 'https://api.mercadolibre.com/users/{}?{}'.format(seller_id, access_token)
        r = requests.get(url).json()
        if r and 'nickname' in r:
            r = r["nickname"]
        return r

    def iterate_df(self, df):
        access_token = self.refresh_token_mercado()
        for item in df:
            for index, row in item.iterrows():
                model = Items()
                site = str(row.get('site'))
                id = str(row.get('id'))
                code = site + id
                item_answer = self.item_request(code, access_token)
                price = int()
                start_time = str()
                name = str()
                description = str()
                nickname = str()
                if item_answer:
                    if "price" in item_answer:
                        price = item_answer["price"]
                    if "start_time" in item_answer:
                        start_time = item_answer["start_time"]
                    if "category_id" in item_answer:
                        name = self.category_request(item_answer["category_id"], access_token)
                    if "currency_id" in item_answer:
                        description = self.currency_request(item_answer["currency_id"], access_token)
                    if "seller_id" in item_answer:
                        nickname = self.seller_request(item_answer["seller_id"], access_token)
                model.id = int(id)
                model.site = site
                model.price = int(price)
                model.start_time = start_time
                model.name = name
                model.description = description
                model.nick_name = nickname
                model.save()
        return

