"""
Csv class.
"""

from app.api.items.entities.Common import CommonEntities
import pandas as pd
from app.constants import ROOT_PATH


class Csv(CommonEntities):
    def __init__(self, file_name, encode, delimiter):
        self.file_name = file_name
        self.encode = encode
        self.delimiter = delimiter

    def read_file(self):
        chunksize = 10 ** 6
        path = '{}/app/{}'.format(ROOT_PATH, self.file_name)
        df = pd.read_csv(path, chunksize=chunksize, iterator=True)
        return df


if __name__ == '__main__':
    CommonEntities()
    entity = Csv(file_name='technical_challenge_data.csv', encode="utf8", delimiter=',')
    entity.read_file()
