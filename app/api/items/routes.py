"""
Routing urls for Items
"""

# third-party imports
from flask import Blueprint
from flask_restful import Api

# local imports
from .resources import ItemResource

items_bp = Blueprint('items', __name__)
api = Api(items_bp)

api.add_resource(
    ItemResource, '/grab_and_save',
    methods=['POST'],
    endpoint='grab_and_save'
)

api.add_resource(
    ItemResource, '/check_task',
    methods=['GET'],
    endpoint='check task'
)
