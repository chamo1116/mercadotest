"""
Helper methods
"""


def response_json(success, data, message=None):
    """
    Helper method that converts the given data in json format
    :param success: status of the APIs either true or false
    :param data: data returned by the APIs
    :param message: user-friendly message
    :return: json response
    """
    data = {
        "response": data,
        "success": success,
        "message": message,
    }
    return data

