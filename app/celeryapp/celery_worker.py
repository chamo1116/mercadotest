"""
    Worker.py Celery
"""

from app.celeryapp import celery_holder
from app.config import app_config
import os
from flask import Flask
from app.db import db


def create_worker_app(config):
    app = Flask(__name__)
    # load configurations
    app.config.from_object(config)
    # initialize app database
    db.init_app(app)

    return app


def config_celery(_app=None):

    TaskBase = celery_holder.celery.Task

    class ContextTask(TaskBase):
        abstract = True

        def __call__(self, *args, **kwargs):
            with _app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)

    celery_holder.celery.conf.update(_app.config)
    celery_holder.celery.Task = ContextTask
    return celery_holder.celery


worker_app = create_worker_app(app_config[os.environ['FLASK_ENV']])
celery_worker = config_celery(worker_app)
