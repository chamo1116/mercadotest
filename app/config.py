"""
Configuration file for the application.
"""

import os
from dotenv import load_dotenv
basedir = os.path.abspath(os.path.dirname(__file__))  # pylint: disable=invalid-name
load_dotenv()


class Config(object):
    """
    Common configurations
    """
    # Put any configurations here that are common across all environments
    DEBUG = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_ECHO = False
    DB_USERNAME = os.environ['MYSQL_USER']
    DB_PASSWORD = os.environ['MYSQL_PASSWORD']
    DB_HOST = os.environ['MYSQL_HOST']
    DATABASE_NAME = os.environ['MYSQL_DATABASE']
    DB_PORT = os.environ['DB_PORT']
    DB_URI = "mysql://%s:%s@%s:%s/%s" % (DB_USERNAME, DB_PASSWORD, DB_HOST, DB_PORT, DATABASE_NAME)
    MYSQL_ROOT_PASSWORD = os.environ['MYSQL_ROOT_PASSWORD']
    SQLALCHEMY_DATABASE_URI = DB_URI
    BROKER_URL = os.getenv('CELERY_BROKER_URL', 'redis://redis:6379/0')
    CELERY_RESULT_BACKEND = os.getenv('CELERY_RESULT_BACKEND', 'redis://redis:6379/0')
    CELERY_TASK_SERIALIZER = 'json'
    CELERY_RESULT_SERIALIZER = 'json'
    CELERY_ENABLE_UTC = True


class ProductionConfig(Config):
    """
    Production configurations
    """
    DEBUG = False
    PORT = 5000


class DevelopmentConfig(Config):
    """
    Development configurations
    """
    DEBUG = True
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    SQLALCHEMY_ECHO = True
    PORT = 3000


app_config = {
    'development': DevelopmentConfig,
    'production': ProductionConfig
}
