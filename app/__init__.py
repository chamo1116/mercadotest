"""
Main application file
"""

# third-party imports
from flask import Flask

# local imports
from . import api
from .db import db
from app.celeryapp.celery_worker import config_celery

# Instantiate Flask extensions
from app import celeryapp


def create_app(config):
    app = Flask(__name__)
    # load configurations
    app.config.from_object(config)

    # initialize app database
    db.init_app(app)
    # initialize api app
    api.init_app(app)

    # celery
    celery = config_celery(app)

    return app
