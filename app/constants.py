"""
Constants used by apps.
"""
import os

# ------------ General Constants -----------------
TEXT_OPERATION_PROCESSING = "Processing Data"
TEXT_OPERATION_SUCCESSFUL = "Data successfully saved"
TEXT_OPERATION_UNSUCCESSFUL = "Operation Unsuccessful"
ROOT_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
