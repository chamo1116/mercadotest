## Summary:
The REST APIs are built using Python Flask framework. Docker containers are used for development environment.

It contains the below mentioned endpoints:
-  POST '/grab_and_save'
-  GET '/check_task'

There are many ways to setup your project folder structure. One is by its function and another is app based structure which means things are grouped bp application. I have used app based approach for this task.

### Python extensions used:
- **flask** - This is a microframework for Python
- **flask_restful** - This is an extension for Flask that adds support for quickly building of REST APIs.
- **flask_script** - This is an extension that provides support for writing external scripts in Flask.
- **flask_migrate** - This is an extension that handles SQLAlchemy database migrations for Flask applications using Alembic.
- **flask_sqlalchemy** - This is an extension for Flask that adds support for SQLAlchemy. It allows to write ORM queries to operate against database.
- **flask_marshmallow** - This is an integration layer for Flask and marshmallow (ORM/ODM/framework-agnostic library for converting complex datatypes, such as objects, to and from native Python datatypes. This is used also to Serializing and Deserializing Objects.) that adds additional features to marshmallow.
- **mysqlclient** - MySQL database connector for Python
- **requests** - This is a python library to make calls to external APIs
- **celery** - This is a python library for asynchronous queue tasks
- **redis** - in-memory data structure store used as message broker
- **pandas** - a library used for data analysis in this case to read and manipulate data
- **pytest** - library used for testing

### How to start application (using Docker)
- Clone the project using command:
    ```
    git clone https://gitlab.com/chamo1116/mercadotest.git
    ```
- Go into the project directory:
    ```
    cd mercadotest
    ```
- Run the application by the following command:
    ```
    make up
    ```

### How to ssh into a Docker containers
- ssh into app container
    ```
    make app-shell
    ```
- ssh into database container
    ```
    make db-shell
    root@e27d5a54fbb8:/# mysql -u test -p
    Enter password: test
    mysql> show databases;
    mysql> use mercadotest;
    mysql> show tables;
    mysql> select * from items;
    ````

### Test using CURL
For test **grab_and_save** endpoint we have to make the next request:
```
- curl --location --request POST 'http://0.0.0.0:5000/api/items/grab_and_save' \
--header 'Content-Type: application/json' \
--data-raw '{
    "format_file": "csv",
    "separator": ",",
    "encode": "utf-8",
    "url_file": "technical_challenge_data.csv"
}'

```

Where:

- **format_file** - It's the file format (for now only **csv** is allowed)
- **separator** - It's the separate symbol (for example **,** for csv file)
- **encode** - It's the encode format (for example **utf-8** for csv file)
- **url_file** - It's the url or path of the file (In this case it's just allowed **technical_challenge_data.csv** because it's file that is in the container)

Answer:

```
{
    "response": {
        "task_id": "3a0b8feb-5aa2-458b-88e5-795a685010f9"
    },
    "success": true,
    "message": "Processing Data"
}

```

If everything is fine you can see in response the **task_id** where you can
check status with the next request:

```
curl --location --request GET 'http://0.0.0.0:5000/api/items/check_task?task_id=3a0b8feb-5aa2-458b-88e5-795a685010f9'
```

Where:

- **task_id** is the id of the task

And the answer is: 

```
{
    "response": {
        "result": "None"
    },
    "success": true,
    "message": "Processing Data"
}

```

Where you can see the state of the task


## Process Explanation

##### English
<div style="text-align: justify"> In this project is used <em>celery</em> as manager tasks because how are many registers
we can't stop the flow of application to wait finishes save the data.
Because of this we did asynchronously using this library and <em>redis</em> as a memory database. 
further we used <em>flower</em> to have a better visualization of tasks states. </div>

##### Spanish
<div style="text-align: justify"> Para este proyecto se utilizó <em>celery</em> como gestor de colas, 
teniendo en cuenta que son cantidades grandes de registros y que el flujo de la aplicación no puede esperar a que 
termine de guardar, se tomó la decisión de realizarlo de manera asíncrona utilizando la librería mencionada y <em>redis</em> 
como intermediario para guardar las tareas. Adicionalmente, se utilizó <em>flower</em> para lograr tener una mejor 
visualización de los estados de las tareas. </div>



# Optimizing process

##### English
<div style="text-align: justify"> To optimize operating system resources, a frame load was used first,
that is, certain registers are brought in, they are operated and so on. For
not load all the complete information in memory but rather small frames.
Additionally, the <em>pandas</em> iterator mode was used to carry out what was explained above and all this
asynchronously as already mentioned
 </div>


##### Spanish
<div style="text-align: justify"> En primer lugar, se utilizó una carga por tramas para optimizar recursos del sistema operativo, 
en otras palabras, es obtener cierta cantidad de regristros para operarlos y repetir el ciclo. Lo anterior, con el fin de no cargar toda la información completa en memoria sino por pequeñas tramas.
Adicionalmente, se utilizó el modo iterador de <em>pandas</em> de forma asíncrona para llevar acabo lo explicado anteriormente. </div>



