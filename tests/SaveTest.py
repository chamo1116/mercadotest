import pytest
from app.tasks import save_data

""" Testing Save Data """


def test_save_data():
    resp = save_data(format_file='csv', url_file='technical_challenge_data.csv', encode="utf8", separator=',')
    assert resp is True, "Save Data successfully"


if __name__ == "__main__":
    pytest.main(["SaveTest.py", "-s"])