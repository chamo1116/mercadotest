import pytest
from app.api.items.entities.Common import CommonEntities

""" Testing Mercado Apis And Token """

CommonEntity = CommonEntities()
ExampleCode = "MLB1332820765"


def test_refresh_token():
    resp = CommonEntity.refresh_token_mercado()
    assert isinstance(resp, str), "Refresh token successfully"


def test_item_request():
    access_token = CommonEntity.refresh_token_mercado()
    resp = CommonEntity.item_request(ExampleCode, access_token)
    assert isinstance(resp, dict), "Good answer"
    assert 'category_id' in resp, "Has category id"
    assert 'error' not in resp, "There weren't errors"


if __name__ == "__main__":
    pytest.main(["MercadoTest.py", "-s"])