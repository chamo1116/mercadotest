"""
Command for running the application
"""
import os

# local imports
from app.config import app_config
from app import create_app

# main entry point of the app
app = create_app(app_config[os.environ['FLASK_ENV']])


if __name__ == '__main__':
    app.run()
